import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class AppPreloadProvider {
    scopeItems: string[] = [];
    childTypes: string[] = [];
    scopeNumber: number = 0;
    childNumber: number = 0;
    isPreloaded: boolean = false;

    constructor(private httpClient: HttpClient) { }

    getChildNumber(): number {
        return this.childNumber;
    }

    getScopeNumber(): number {
        return this.scopeNumber;
    }

    getScopeItems(): string[] {
        return this.scopeItems;
    }

    getChildTypes(): string[] {
        return this.childTypes;
    }

    preload() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                this.scopeItems = ["Nagy mozgás", "Figyelem", "Emlékezet"];
                this.childTypes = [
                    "SNI", "BTMN", "Csoport Grafó", "Csoport iskola", "egyéb"
                ]
                this.scopeNumber = this.scopeItems.length;
                this.childNumber = this.childTypes.length;
                console.log('preload complete')
                this.isPreloaded = true;
                resolve(true);
            }, 1000)


        });
    }
}