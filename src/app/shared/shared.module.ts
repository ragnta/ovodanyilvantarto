import { NgModule } from "@angular/core";
import { ScopeNamePipe } from "./scopename.pipe";

@NgModule({
    declarations: [ScopeNamePipe],
    exports: [ScopeNamePipe]
})
export class SharedModule {

}