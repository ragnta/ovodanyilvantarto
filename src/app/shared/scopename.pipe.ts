import { Pipe, PipeTransform } from "@angular/core";
import { AppPreloadProvider } from "./app-preload.provider";





@Pipe({
    name: 'scopename'
})
export class ScopeNamePipe implements PipeTransform {

    scopes: string[];

    constructor(private appPreloadProvider: AppPreloadProvider) {
        this.scopes = this.appPreloadProvider.getScopeItems();
    }

    transform(value: any) {
        return this.scopes[+value];
    }
}