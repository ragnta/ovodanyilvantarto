import { Component, OnInit } from '@angular/core';
import { Child } from '../models/child.model';
import { ChildService } from '../child.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-child-list',
  templateUrl: './child-list.component.html',
  styleUrls: ['./child-list.component.css']
})
export class ChildListComponent implements OnInit {

  childList: Child[];
  subscription: Subscription;

  constructor(private cService: ChildService) { }

  ngOnInit() {
    this.childList = this.cService.getChildren();
    this.subscription = this.cService.childChanged.subscribe((children: Child[]) => {
      this.childList = children;
    });

  }

}
