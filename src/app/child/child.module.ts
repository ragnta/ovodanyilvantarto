import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChildRoutingModule } from "./child-routing.module";
import { ChildListComponent } from "./child-list/child-list.component";
import { ChildDetailComponent } from "./child-detail/child-detail.component";
import { ChildEditComponent } from "./child-edit/child-edit.component";
import { ScopeNamePipe } from "../shared/scopename.pipe";
import { StartComponent } from './start/start.component';
import { ChildTypePipe } from "./childtype.pipe";
import { SharedModule } from "../shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { ChildAuthGuard } from "./child-auth-guard.service";

@NgModule({
    declarations: [
        ChildListComponent,
        ChildDetailComponent,
        ChildEditComponent,
        StartComponent,
        ChildTypePipe
    ],
    imports: [
        CommonModule,
        ChildRoutingModule,
        SharedModule,
        ReactiveFormsModule
    ],
    providers: [ChildAuthGuard]

})
export class ChildModule {

}