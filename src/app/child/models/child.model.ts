
import { Oppurtinity } from "../../oppurtinities/models/oppurtinity.model";

export class Child {

    constructor(public name: string, public type: number, public birthyear: number, public group: string, public description: string, public scopes: number[]) { }

    public getAge(): number {
        return new Date().getFullYear() - this.birthyear;
    }
}