import { Pipe, PipeTransform } from "@angular/core";
import { AppPreloadProvider } from "../shared/app-preload.provider";

@Pipe({
    name: 'childtype'
})
export class ChildTypePipe implements PipeTransform {

    types: string[];

    constructor(private appPreloadProvider: AppPreloadProvider) {
        this.types = this.appPreloadProvider.getChildTypes();
    }

    transform(value: any) {
        return this.types[+value];
    }
}