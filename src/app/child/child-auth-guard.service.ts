import { CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { Injectable } from "@angular/core";
import { ChildService } from "./child.service";

@Injectable()
export class ChildAuthGuard implements CanActivate {

    constructor(private childService: ChildService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {

        if (route.params['id'] != null) {
            const child = this.childService.getChildById(+route.params['id']);
            if (child == null) {
                this.router.navigate(['../../error']);
            }
        }
        return true;
    }
}