import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Child } from '../models/child.model';
import { ChildService } from '../child.service';
import { Oppurtinity } from '../../oppurtinities/models/oppurtinity.model';
import { OppurtinitiesService } from '../../oppurtinities/oppurtinities.service';

@Component({
  selector: 'app-child-detail',
  templateUrl: './child-detail.component.html',
  styleUrls: ['./child-detail.component.css']
})
export class ChildDetailComponent implements OnInit {

  childId: number;
  child: Child;
  oppurtinities: Oppurtinity[];

  constructor(private router: Router, private route: ActivatedRoute, private cService: ChildService, private oppurtinityService: OppurtinitiesService) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      if (params['id']) {
        this.childId = +params['id'];
        this.child = this.cService.getChildById(this.childId);
        this.oppurtinities = this.oppurtinityService.getOppurtinitiesByMultipleScope(this.child.scopes);
      }
    })
  }

  onClickEdit() {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  onClickBack() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
