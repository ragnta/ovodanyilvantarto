import { Component, OnInit } from '@angular/core';
import { Child } from '../models/child.model';
import { ChildService } from '../child.service';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-child-edit',
  templateUrl: './child-edit.component.html',
  styleUrls: ['./child-edit.component.css']
})
export class ChildEditComponent implements OnInit {
  childId: number;
  types = [0, 1, 2, 3];
  scopeList = [0, 1, 2];
  editMode: boolean = true;
  childForm: FormGroup;

  constructor(private router: Router, private route: ActivatedRoute, private childService: ChildService) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.childId = +params['id'];
      this.editMode = params['id'] != null;
      this.initForm();
    })

  }

  private initForm() {
    let childName = '';
    let childType = 0;
    let childDescription = '';
    let childGroup = '';
    let childScope = new FormGroup({});
    for (let i = 0; i < this.scopeList.length; i++) {
      childScope.addControl('' + i, new FormControl(false));
    }
    console.log()

    let childBirthyear = 0;

    if (this.editMode) {
      const child = this.childService.getChildById(this.childId);
      childName = child.name;
      childType = child.type;
      childDescription = child.description;
      childGroup = child.group;
      childBirthyear = child.birthyear
      if (child['scopes']) {
        for (let i = 0; i < this.scopeList.length; i++) {
          const hasScope = child.scopes.indexOf(this.scopeList[i]) !== -1;
          if (hasScope) {
            childScope.get('' + i).setValue(true);
          }
        }
      }
    }
    this.childForm = new FormGroup({
      'name': new FormControl({ value: childName, disabled: this.editMode }, Validators.required),
      'type': new FormControl(childType, Validators.required),
      'description': new FormControl(childDescription, Validators.required),
      'group': new FormControl(childGroup, Validators.required),
      'childScopeList': childScope,
      'birthyear': new FormControl({ value: childBirthyear, disabled: this.editMode }, Validators.required)
    });
  }

  onSubmit() {
    const newChild = new Child(this.childForm.value['name'], this.childForm.value['type'], this.childForm.value['birthyear'], this.childForm.value['group'], this.childForm.value['description'], this.getScopes(this.childForm.value['childScopeList']))
    console.log(newChild);
    if (this.editMode) {
      this.childService.updateChild(this.childId, newChild);
    } else {
      this.childService.createChild(newChild);
    }
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  onClickBack() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  getScopes(scopeFg: FormGroup) {
    let result = [];
    console.log(scopeFg);
    for (let i = 0; i < this.scopeList.length; i++) {
      if (scopeFg['' + i] === true) {
        result.push(i);
      }
    }
    return result;
  }
}
