import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { Child } from "./models/child.model";


export class ChildService {
    childChanged = new Subject<Child[]>();
    childList: Child[] = [
        new Child('Jakab', 0, 2015, 'Katica', 'Jakab fél egyedül', [1, 2]),
        new Child('Géza', 3, 2012, 'Táncoló pingvinek', 'Géza szereti a kekszet', [0, 2]),
    ]

    getChildren() {
        return this.childList.slice();
    }

    getChildById(index: number) {
        return this.childList[index];
    }

    updateChild(index: number, child: Child) {
        const oldChild = this.childList[index];
        child.name = oldChild.name;
        child.birthyear = oldChild.birthyear;
        this.childList[index] = child;
    }

    createChild(child: Child) {
        this.childList.push(child);
        this.childChanged.next(this.childList.slice());
    }

}