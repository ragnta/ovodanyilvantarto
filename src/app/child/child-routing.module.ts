import { Routes, RouterModule, PreloadAllModules } from '@angular/router'
import { NgModule } from '@angular/core';
import { ChildListComponent } from './child-list/child-list.component';
import { ChildDetailComponent } from './child-detail/child-detail.component';
import { ChildEditComponent } from './child-edit/child-edit.component';
import { StartComponent } from './start/start.component';
import { ChildAuthGuard } from './child-auth-guard.service';

const appRoutes: Routes = [
    {
        path: '', component: ChildListComponent, children: [
            {
                path: '', component: StartComponent
            },
            {
                path: 'new', component: ChildEditComponent
            },
            {
                path: ':id', component: ChildDetailComponent, canActivate: [ChildAuthGuard]
            },
            {
                path: ':id/edit', component: ChildEditComponent, canActivate: [ChildAuthGuard]
            },

        ]
    }
]
@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule]
})
export class ChildRoutingModule {

}