import { Routes, RouterModule, PreloadAllModules } from '@angular/router'
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { ErrorComponent } from './error/error.component';
import { ForcePreloadStrategy } from './force.preloadstrategy';


const appRoutes: Routes = [
    {
        path: '', component: HomeComponent
    },
    {
        path: 'children', loadChildren: './child/child.module#ChildModule', data: { preload: true }
    },
    {
        path: 'oppurtinities', loadChildren: './oppurtinities/oppurtinities.module#OppurtinitiesModule', data: { preload: true }
    },
    {
        path: 'error', component: ErrorComponent
    }
]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, { preloadingStrategy: ForcePreloadStrategy })],
    exports: [RouterModule],
    providers: [ForcePreloadStrategy]
})
export class AppRoutingModule {

}