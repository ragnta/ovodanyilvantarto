import { NgModule } from "@angular/core";
import { OppurtinitiesListComponent } from "./oppurtinities-list/oppurtinities-list.component";
import { OppurtinitiesDetailComponent } from "./oppurtinities-detail/oppurtinities-detail.component";
import { CommonModule } from "@angular/common";
import { OppurtinitiesRoutingModule } from "./oppurtinities-routing.module";
import { StartComponent } from './start/start.component';
import { FormsModule } from "@angular/forms";
import { OppurtinitiesNewComponent } from "./oppurtinities-new/oppurtinities-new.component";
import { ScopeNamePipe } from "../shared/scopename.pipe";
import { SharedModule } from "../shared/shared.module";
import { OppurtinitiesAuthGuard } from "./oppurtinities-auth-guard.service";

@NgModule({
    declarations: [
        OppurtinitiesListComponent,
        OppurtinitiesNewComponent,
        OppurtinitiesDetailComponent,
        StartComponent
    ],
    imports: [
        CommonModule,
        OppurtinitiesRoutingModule,
        FormsModule,
        SharedModule
    ],
    providers: [OppurtinitiesAuthGuard]
})
export class OppurtinitiesModule {

}