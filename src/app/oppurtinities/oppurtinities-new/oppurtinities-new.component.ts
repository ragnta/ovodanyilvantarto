import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Oppurtinity } from '../models/oppurtinity.model';
import { OppurtinitiesService } from '../oppurtinities.service';

@Component({
  selector: 'app-oppurtinities-new',
  templateUrl: './oppurtinities-new.component.html',
  styleUrls: ['./oppurtinities-new.component.css']
})
export class OppurtinitiesNewComponent implements OnInit {

  id: number;
  constructor(private router: Router, private route: ActivatedRoute, private oService: OppurtinitiesService) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      if (params['id']) {
        this.id = +params['id'];
      }
    })
  }

  onSubmit(form: NgForm) {
    const value = form.value;
    const oppurtinity = new Oppurtinity(this.id, value.name);
    this.oService.addOppurtinity(oppurtinity);
    form.resetForm();
    this.onBackClick();
  }

  onBackClick() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
