import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Oppurtinity } from '../models/oppurtinity.model';
import { OppurtinitiesService } from '../oppurtinities.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-oppurtinities-detail',
  templateUrl: './oppurtinities-detail.component.html',
  styleUrls: ['./oppurtinities-detail.component.css']
})
export class OppurtinitiesDetailComponent implements OnInit, OnDestroy {


  id: number;
  oppurtinitiesList: Oppurtinity[];
  subscription: Subscription;
  isChanged: boolean = false;

  constructor(private route: ActivatedRoute, private router: Router, private oService: OppurtinitiesService) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.isChanged = !this.oService.isSaved;
      this.oppurtinitiesList = this.oService.getOppurtinitiesByScopeId(+this.id);
    })
    this.subscription = this.oService.oppurtinitiesChanged.subscribe((oppurtinities: Oppurtinity[]) => {
      this.isChanged = !this.oService.isSaved;
      this.oppurtinitiesList = oppurtinities;
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onClickBack() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  onClickEdit() {
    this.router.navigate(['new'], { relativeTo: this.route });
  }

  deleteOppurtinity(oppurtinity: Oppurtinity) {
    this.oService.removeOppurtinity(oppurtinity);
  }

  onSave() {
    this.oService.saveOppurtinities(+this.id);
  }
}
