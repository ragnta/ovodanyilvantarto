import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OppurtinitiesDetailComponent } from './oppurtinities-detail.component';

describe('OppurtinitiesDetailComponent', () => {
  let component: OppurtinitiesDetailComponent;
  let fixture: ComponentFixture<OppurtinitiesDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OppurtinitiesDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OppurtinitiesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
