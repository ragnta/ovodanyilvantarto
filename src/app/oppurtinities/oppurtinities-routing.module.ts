import { Routes, RouterModule, PreloadAllModules } from '@angular/router'
import { NgModule } from '@angular/core';
import { OppurtinitiesListComponent } from './oppurtinities-list/oppurtinities-list.component';
import { OppurtinitiesDetailComponent } from './oppurtinities-detail/oppurtinities-detail.component';
import { StartComponent } from './start/start.component';
import { OppurtinitiesNewComponent } from './oppurtinities-new/oppurtinities-new.component';
import { OppurtinitiesAuthGuard } from './oppurtinities-auth-guard.service';


const appRoutes: Routes = [
    {
        path: '', component: OppurtinitiesListComponent, children: [
            {
                path: '', component: StartComponent
            },
            {
                path: ':id', component: OppurtinitiesDetailComponent, canActivate: [OppurtinitiesAuthGuard]
            },
            {
                path: ':id/new', component: OppurtinitiesNewComponent, canActivate: [OppurtinitiesAuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule]
})
export class OppurtinitiesRoutingModule {

}