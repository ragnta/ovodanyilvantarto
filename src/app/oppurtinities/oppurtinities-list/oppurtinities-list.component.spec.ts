import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OppurtinitiesListComponent } from './oppurtinities-list.component';

describe('OppurtinitiesListComponent', () => {
  let component: OppurtinitiesListComponent;
  let fixture: ComponentFixture<OppurtinitiesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OppurtinitiesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OppurtinitiesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
