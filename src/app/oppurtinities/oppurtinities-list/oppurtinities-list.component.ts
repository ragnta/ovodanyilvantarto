import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-oppurtinities-list',
  templateUrl: './oppurtinities-list.component.html',
  styleUrls: ['./oppurtinities-list.component.css']
})
export class OppurtinitiesListComponent implements OnInit {


  scopeList: number[] = [0, 1, 2];
  subscription: Subscription;

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

  }

  onScopeItemClick(index: number) {
    this.router.navigate(['' + index], { relativeTo: this.route });
  }
}
