import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { AppPreloadProvider } from "../shared/app-preload.provider";
import { Injectable } from "@angular/core";

@Injectable()
export class OppurtinitiesAuthGuard implements CanActivate {

    constructor(private applicationPreloadService: AppPreloadProvider, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        console.log(route.params['id']);
        console.log(this.applicationPreloadService.getScopeNumber());
        if (route.params['id'] != null) {

            if (this.applicationPreloadService.getScopeNumber() - 1 < +route.params['id']) {
                this.router.navigate(['../../error']);
            }
        }
        return true;

    }
}