import { Subject } from "rxjs/Subject";
import { Oppurtinity } from "./models/oppurtinity.model";

export class OppurtinitiesService {
    oppurtinitiesChanged = new Subject<Oppurtinity[]>();
    isSaved = true;

    constructor() { }
    private oppurtinities: Oppurtinity[] = [
        new Oppurtinity(0, 'Lépcső'),
        new Oppurtinity(0, 'Kuszás'),
        new Oppurtinity(1, 'Kakukktojás'),
        new Oppurtinity(2, 'memoriakártya')
    ]

    getOppurtinitiesByScopeId(scopeId: number): Oppurtinity[] {
        return this.oppurtinities.slice().filter((opp) => {
            if (opp.scopeId === scopeId) {
                return true;
            }
        })
    }

    addOppurtinity(oppurtinity: Oppurtinity) {
        this.oppurtinities.push(oppurtinity);
        this.isSaved = false;
        this.oppurtinitiesChanged.next(this.oppurtinities.slice());
    }

    removeOppurtinity(oppurtinity: Oppurtinity) {
        const scopeId = oppurtinity.scopeId;
        if (this.oppurtinities.indexOf(oppurtinity) !== -1) {
            this.oppurtinities.splice(this.oppurtinities.indexOf(oppurtinity), 1);
        }
        this.isSaved = false;
        this.oppurtinitiesChanged.next(this.getOppurtinitiesByScopeId(scopeId));
    }

    getOppurtinitiesByMultipleScope(scopeIdList: number[]) {
        return this.oppurtinities.slice().filter((opp) => {
            for (let scopeId of scopeIdList) {
                if (opp.scopeId === scopeId) {
                    return true;
                }
            }

        })


    }

    saveOppurtinities(scopeId: number) {
        this.isSaved = true;
        this.oppurtinitiesChanged.next(this.getOppurtinitiesByScopeId(scopeId));
    }
}