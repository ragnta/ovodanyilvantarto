import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { OppurtinitiesService } from './oppurtinities/oppurtinities.service';
import { ScopeNamePipe } from './shared/scopename.pipe';
import { SharedModule } from './shared/shared.module';
import { ChildService } from './child/child.service';
import { ErrorComponent } from './error/error.component';
import { AppPreloadProvider } from './shared/app-preload.provider';
import { HttpClientModule } from '@angular/common/http';


export function appPreloadProviderFactory(provider: AppPreloadProvider) {
  return () => provider.preload();
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    SharedModule
  ],
  providers: [AppPreloadProvider,
    { provide: APP_INITIALIZER, useFactory: appPreloadProviderFactory, deps: [AppPreloadProvider], multi: true },
    OppurtinitiesService, ChildService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
